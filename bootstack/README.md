# Switch bootstack

## Understanding the Nintendo Switch boot stack

### Prerequisites

- [Vulnerable Switch](https://gbatemp.net/threads/firmware-status.495078/)
- Micro SDCard (8GB Minimum)

### Micro SD Card Setup

- Format your SD Card, you need at least : \
   &nbsp;Partition 1 for bootfiles and bootloader: \
      &nbsp;&nbsp;type: vfat

   &nbsp;Partition 2 for Gentoo root: \
      &nbsp;&nbsp;type: ext4

- Grab the latest Hekate release, [here](https://github.com/CTCaer/hekate/releases/) and extract it to `vfat:/`
- Note: Hekate is able to resize vfat partition and create new partitions for Linux. The creating of ext4 file system is done during installation

### Bootchain explanation

1. **Hekate** uses the config files in `vfat:/bootloader/ini/*.ini` for boot entries. If a boot entry is selected, Hekate then executes the payload set in this `.ini` file, an example here with *Gentoo*: `payload=switchroot/gentoo/coreboot.rom`

2. **Coreboot** is the **BIOS**. Inside of the Coreboot the **U-boot** bootloader is built in.

3. **U-boot** loads the configuration file `vfat:switchroot/gentoo/boot.scr`. Every distro provides his own coreboot.rom with modified u-boot pointing to the correct **switchroot/*distro*** folder.

4. The **boot.scr** or *U-boot script*, is responsible of booting the linux kernel. The process is the following :
   1. load linux kernel `Image` into memory,
   2. load `tegra210-icosa.dtb` into memory,
   3. load `initramfs` (if provided) into memory,
   4. load then read the parameters set in uenv.txt file
   5. load configured overlays
   6. boot the linux kernel in the prepared environment

5. If an **initramfs** is provided, the Kernel uses it as *initial root*. The initramfs does the next magic:
   1. Resize the root partition if necessary on first boot
   2. Extract/Install the kernel modules, `modules.tar.gz`, into `root:/lib/modules` if provided (external kernel upgrade)
   3. Install `update.tar.gz` into `vfat:switchroot/distro` if provided
   4. Generates bluetooth pairing cache based on provided `vfat:switchroot/joycon_mac.ini`
   5. Do some updates on root
   5. Initialize the framebuffer
   6. Change the root into prepared root partition

*Note: If no initramfs is provided, the kernel runs directly on root partition without preparations*

6. The future processing is depending on distro and used init system. Linux boots!

## Building

## Kernel

[Kernel build script](https://gitlab.com/switchroot/kernel/l4t-kernel-build-scripts)

Like for u-boot we provide two ways for building our kernel, a docker image and compiling the sources like any other C projects.

## Uboot

[Switch-Uboot](https://gitlab.com/switchroot/bootstack/switch-uboot)

You can build u-boot in two ways, using our docker image or compiling the sources like any other C projects.

### Requirements

GCC 7.4.0 or (Linaro GCC 7.4.0)[https://releases.linaro.org/components/toolchain/binaries/7.4-2019.02/aarch64-linux-gnu/] if you're cross compiling
`sudo apt-get install python python-dev bc curl`

### Configuring uboot scripts location

In `include/config_distro_bootcmd.h` change the `boot_prefixes=` to use the correct path where uboot scripts are located.

### Building

`make nintendo-switch_defconfig`
`make`

## Coreboot

[Coreboot-Switch](https://gitlab.com/switchroot/bootstack/coreboot-switch)

### Requirements

GCC 7.X.X

### Pre build

Coreboot requires to have arm and aarch64 compilers as the switch is both arm and arm64 architecture.
You have 2 choice, either compile coreboot cross compilers or use Linaro pre built cross compilers.
If you chose to build corebooot's cross compilers follow this :

Building arm cross compiler :

`make crossgcc-arm`

Building aarch64 cross compiler :

`make crossgcc-aarch64`

### Building

`make nintendo_switch_defconfig`
`make iasl`

**Use only 2 threads as it won't build with more threads.**
`make -j2`

## Debugging

### UART B

The Switch has 5 classic UART 1.8v interfaces.
2 of them are used for comms with joycon.

### Requirements

We highly recommand taking a 1.8v UART -> USB adapter otherwise you won't be able to use TX from the switch

- DSD TECH SH-U09C5 ( 1.8v UART -> USB )
- Right Joy-con rail

### Wiring UART B

TBC

### USB UART

For USB UART enable the following options in kernel :

```txt
CONFIG_USB_G_SERIAL=y
CONFIG_U_SERIAL_CONSOLE=y
CONFIG_USB_SERIAL_CONSOLE=y
```

Then make sure all configfs gadgets are disabled and rebuild the kernel.