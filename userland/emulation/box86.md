# Box86

## Introduction

Running Linux version of Steam using box86 using chroot and install Wine.

Parts of this guide are based on this article by rna from armbian forum https://forum.armbian.com/topic/16584-install-box86-on-arm64/

## Setting up

At the beginning you need to prepare armhf chroot with Mesa's virpipe.

1. Open terminal, then type:

```
sudo apt install schroot debootstrap
sudo mkdir -p /srv/chroot/debian-armhf
sudo debootstrap --arch armhf --foreign bullseye /srv/chroot/debian-armhf http://deb.debian.org/debian
sudo chroot "/srv/chroot/debian-armhf" /debootstrap/debootstrap --second-stage
```

2. Then add a config file for debian-armhf: 

```
sudo nano /etc/schroot/chroot.d/debian-armhf.conf
```

Copy paste this code, and change <username> into computer username.

```
[debian-armhf]
description=Debian Armhf chroot
aliases=debian-armhf
type=directory
directory=/srv/chroot/debian-armhf
profile=desktop
personality=linux
preserve-environment=true
root-users=<username>
users=<username>
```

3. Then edit the nssdatabases 

```
sudo nano /etc/schroot/desktop/nssdatabases
```

copy paste this code 

```
# System databases to copy into the chroot from the host system.
#
# <database name>
#passwd
shadow
#group
gshadow
services
protocols
#networks
#hosts
#user
```

4. Edit the stateoverride, and change the first contrab to root 

```
sudo nano /srv/chroot/debian-armhf/var/lib/dpkg/statoverride
```

copy paste this code

```
root root 2755 /usr/bin/crontab
```

5. Modify chroot fstab to not mount home directory by editing file 

```
nano /etc/schroot/desktop/fstab
```

add "#" before line

```
/home           /home           none    rw,bind         0       0
```

6. Now you should be able to schroot by typing 

```
sudo schroot -c debian-armhf
```

7. edit the bashrc. If you want to use Xephyr on other port than 2, change that number.

```
nano ~/.bashrc
```

add this code to the bottom of the line 

```
export LANGUAGE="C"
export LC_ALL="C"
export DISPLAY=:2
export LIBGL_ALWAYS_SOFTWARE=1
export GALLIUM_DRIVER=virpipe
```

8. let's restart chroot environment by exit and login again, install sudo, then add a username that is similar to your main system username and give that user sudo privileges

```
echo "<username> ALL=(ALL:ALL) ALL" >> /etc/sudoers
apt install sudo
adduser <username>
usermod -aG sudo <username>
su - <username>
```

then again add bashrc 

```
nano ~/.bashrc
```

add this code to the bottom of the line 

```
export LANGUAGE="C"
export LC_ALL="C"
export DISPLAY=:2
export LIBGL_ALWAYS_SOFTWARE=1
export GALLIUM_DRIVER=virpipe
```

9. Then restart chroot by double exit, login again, add deb-src source to apt and buster source to have libappindicator1 in sources and then install the following packages 

```
exit
exit
schroot -c debian-armhf
sudo bash -c 'echo "deb-src http://deb.debian.org/debian bullseye main" >> /etc/apt/sources.list'
sudo bash -c 'echo "deb http://deb.debian.org/debian buster main" >> /etc/apt/sources.list'
sudo apt update && sudo apt upgrade
sudo apt install git wget cmake build-essential meson python3 gcc pciutils
sudo apt build-dep mesa
```

10. Now let's compile the Box86 within Chroot environment. Usage of RPI4 flags works fine on my Nano so Switch should also works fine. It have some additional workarounds.

```
git clone https://github.com/ptitSeb/box86

cd box86

mkdir build; cd build; cmake .. -DRPI4=1; make; sudo make install
```

It's time to compile and install Mesa driver. For me, 20.1 version was working best, new versions have issues with some games.

```
git clone https://gitlab.freedesktop.org/mesa/mesa -b 20.1
cd mesa; meson build -Dgallium-drivers=virgl,swrast -Dvulkan-drivers=; cd build; ninja; sudo ninja install
```

Before we proceed further in chroot, we need to copy binfmt.d config from chroot to host and restart systemd-binfmt to make box86 works automaticly with bash scripts that includes i386 files.

```
exit
sudo cp /srv/chroot/debian-armhf/etc/binfmt.d/box86.conf /etc/binfmt.d/box86.conf
sudo systemctl restart systemd-binfmt
```

Also we need to install virgl-test-server to have GPU acceleration.

```
sudo apt install virgl-server
```

11. Time to install steam. Before we go to chroot, you need to start Xephyr virgl_test_server. Use below command in separate terminal.

```
Xephyr :2 & virgl_test_server --use-glx
```

With them running, go back to main terminal and use command below.

```
schroot -c debian-armhf
sudo apt install libnm0 zenity nginx libgtk2.0-0 libdbus-glib-1-2 libxss1 libdbusmenu-gtk4 libsdl2-2.0-0 libice6 libsm6 libopenal1 libusb-1.0-0 libappindicator1
mkdir steam; cd steam; wget http://media.steampowered.com/client/installer/steam.deb; ar x steam.deb; tar xf data.tar.xz; STEAMOS=1 setarch -L linux32 ./usr/lib/steam/bin_steam.sh
```

You should be able to run steam in chroot with command ```STEAMOS=1 setarch -L linux32 ~/steam/usr/lib/steam/bin_steam.sh```

In Xephyr window you should see Steam. Browser won't work as It's 64-bit, in view choose small mode an this part should work. Keep in mind that It's unstable so it can crash.

About Wine, you can try to just extract it as in original guide linked at the beginning, but there is also more flawless way. Only iisue is that there is a chance that it would break chroot in future.

If you want to try it, use commands below while in chroot.

```
sudo dpkg --add-architecture i386
wget -nc https://dl.winehq.org/wine-builds/winehq.key
sudo apt-key add winehq.key
sudo bash -c 'echo "deb https://dl.winehq.org/wine-builds/debian/ bullseye main " >> /etc/apt/sources.list'
sudo apt update
sudo apt install wine-stable:i386
```

This way you can install any Wine version you want from their main repo.

Every time you start app from this chroot you need to have Xephyr and virgl_test_server running with command from earlier. You can define Xephyr resolution with -display variable.
